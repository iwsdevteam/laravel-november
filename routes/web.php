<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('teszt', function (){
    return 'Hello World';
});
Route::get('rolam','PageController@about')->name('about');//nevesített route route('név')
Route::get('kapcsolat','PageController@contact')->name('contact');
Route::post('kapcsolat','PageController@send')->name('contact-send');

//cikkek routjai
Route::get('cikkek','ArticlesController@index')->name('articles');
Route::get('cikkek/uj','ArticlesController@create')->name('article.create');//űrlap
Route::post('cikkek/uj','ArticlesController@store');//feldolgozása
Route::get('cikkek/make/{db?}','ArticlesController@make')->middleware('auth');//kötegelt cikkgyártás dummy tartalommal

Route::get('cikk/{id?}','ArticlesController@show');//Egy cikk mutatása
Route::get('cikk/edit/{id?}','ArticlesController@edit')->middleware('auth');//Egy cikk szerkesztése , middleware védelemmel
Route::post('cikk/update/{id?}','ArticlesController@update')->name('article.update'); //cikk módosítása
Route::delete('cikk/delete/{id?}','ArticlesController@destroy');//Egy cikk törlése
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
