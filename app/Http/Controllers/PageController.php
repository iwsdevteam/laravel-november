<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class PageController extends Controller {

    public function about() {
        $lastName = 'Horváth';
        $firstName = 'György';
        $i = rand(0, 1);
        //return view('about',['firstName'=>$firstName,'lastName'=>$lastName]);
        //return view('about')->with('firstName',$firstName)->with('lastName',$lastName);
        return view('about', compact('lastName', 'firstName', 'i'));
    }

    public function contact() {
        return view('contact');
    }

    public function send(Request $request) {
        $validator = $request->validate([
            'name' => 'required|min:3',
            'email' => 'required|email',
            'msg' => 'min:10',
        ]);
        //ide csak akkor jutunk ha a validálás sikeres
        Mail::send('emails.contact-email', ['data'=>$request->toArray()], function ($m) use($request) {
            $m->from(env('MAIL_CONTACT','hgy@ruander.hu'), $request->get('name'));
            $m->replyTo($request->get('email'),$request->get('name'));
            $m->to(env('MAIL_CONTACT','hgy@ruander.hu'), env('APP_NAME'))->subject('Kapcsolati megkeresés.');
        });
        dd($request->all());
    }

}
