<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article; //model
//idővel kapcsolatos bármi
use Carbon\Carbon;
use App\Http\Requests\UpdateArticleRequest; //request bekötése
use Auth;
use Faker;

class ArticlesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
//        dump(Auth::check());
        $articles = Article::orderBy('publish_on','DESC')->where('publish_on', '<=' , Carbon::now() )->get();
        return view('articles.index', compact('articles'));
    }

    /**
     * Kötegelt cikk gyártása a Faker használatával
     * @param int $db
     */
    public function make($db = 1) {

        $faker = Faker\Factory::create();
        for ($i = 1; $i <= $db; $i++) {
            $data = [
                'title' => $faker->sentence(),
                'lead' => $faker->sentences(5, true),
                'content' => $faker->text(400),
                'publish_on' => $faker->dateTimeBetween('-30 days', '+14 days'),
            ];
            $article = new Article($data);
            $article->save();
        }
        return redirect()->to('cikkek');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = $request->validate([
            'title' => 'required',
        ]);

        $article = new Article($request->all());
        //ideiglenesen mivel nem formból jön a publish on property
        $article->publish_on = Carbon::now();
        $article->save();
        return redirect()->to('cikkek');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id = 0) {
        $article = Article::find($id);
        if (!$article) {
            //abort(404); 
            return redirect()->to('cikkek');
        }
        dd($article); //teljes cikk mutatása (HF)
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $article = Article::find($id);
        if ($article) {
            return view('articles.update', compact('article'));
        }
        dd('nem találtuk a cikket: ' . $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateArticleRequest $request, $id) {
        //mentés és vissza a listázásra

        $article = Article::find($id); //cikk lekérése
        $article->update($request->all()); //módosítás

        return redirect('cikkek'); //vissza a cikkekre
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $article = Article::find($id); //cikk lekérése
        $article->delete(); //törlés
        return redirect()->back(); //vissza a listára
    }

}
