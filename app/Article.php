<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Article extends Model
{
    protected $fillable = [
      'title','lead','content','publish_on'  
    ];
    
    public function getPublishOnAttribute($value){
//       dd(Carbon::now());
        return 'helo:'.$value;
    }
}
