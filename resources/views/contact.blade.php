@extends('layouts.master')

@section('page-title','kapcsolat oldal')

@section('content')
    kapcsolat űrlap
   @include('errors.all')
   {!! Form::open() !!}
   <div class="form-group">
   {!! Form::label('name', 'Név') !!}
   {!! Form::text('name','Mogyoró',['class'=>'form-control','placeholder'=>'John Doe']) !!}
   </div>
   <div class="form-group">
   {!! Form::label('email', 'Email') !!}
   {!! Form::email('email',null,['class'=>'form-control','placeholder'=>'email@cim.hu']) !!}
   </div>
   <div class="form-group">
   {!! Form::label('msg', 'Üzenet') !!}
   {!! Form::textarea('msg',null,['class'=>'form-control','placeholder'=>'Írd ide az üzenetet!']) !!}
   </div>
   {!! Form::submit('Elküld',['class'=>'btn btn-primary']) !!}
   {!! Form::close() !!}
@endsection