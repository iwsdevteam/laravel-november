<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ env('APP_NAME', 'Default Name') }} | @yield('page-title')</title>          <link href="{{asset('css/app.css')}}" type="text/css" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
            @include('partials.header')
        </div>
        <div class="container">
            @yield('content')
        </div>
        <div class="container-fluid">
            @include('partials.footer')
        </div>
        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>
