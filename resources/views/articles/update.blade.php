@extends('layouts.master')

@section('content')
@include('errors.all')
{!! Form::model($article, ['route' => ['article.update', $article->id]]) !!}
<div class="form-group">
    {!! Form::label('title', 'Cím') !!}
    {!! Form::text('title',old('title',$article->title),['class'=>'form-control','placeholder'=>'Lorem ipsum']) !!}
</div>
<div class="form-group">
    {!! Form::label('lead', 'Bevezető') !!}
    {!! Form::textarea('lead',old('lead',$article->lead),['class'=>'form-control','placeholder'=>'Lorem ipsum dolor sit amet.']) !!}
</div>
<div class="form-group">
    {!! Form::label('content', 'Tartalom') !!}
    {!! Form::textarea('content',old('content',$article->content),['class'=>'form-control','placeholder'=>'Lorem ipsum dolor sit amet.']) !!}
</div>
{!! Form::submit('Mehet') !!}
{!! Form::close() !!}


@endsection