@extends('layouts.master')

@section('content')
@include('errors.all')
{!! Form::open() !!}
<div class="form-group">
    {!! Form::label('title', 'Cím') !!}
    {!! Form::text('title',null,['class'=>'form-control','placeholder'=>'Lorem ipsum']) !!}
</div>
<div class="form-group">
    {!! Form::label('lead', 'Bevezető') !!}
    {!! Form::textarea('lead',null,['class'=>'form-control','placeholder'=>'Lorem ipsum dolor sit amet.']) !!}
</div>
<div class="form-group">
    {!! Form::label('content', 'Tartalom') !!}
    {!! Form::textarea('content',null,['class'=>'form-control','placeholder'=>'Lorem ipsum dolor sit amet.']) !!}
</div>
{!! Form::submit('Mehet') !!}
{!! Form::close() !!}


@endsection