@extends('layouts.master')

@section('page-title','Cikkek')

@section('content')
@auth
<a class="btn btn-success" href="{{route('article.create')}}">Új cikk</a>
@endauth
@foreach($articles as $article)
<article>
    <h1><a href="{{url('cikk',$article->id)}}">{{$article->title}}</a></h1>
    <section>{{$article->lead}}</section>
    <footer><time datetime="{{$article->publish_on}}">{{$article->publish_on}}</time></footer>
    @auth
    <div class="actions">
        <a class="btn btn-warning" href="{{url('cikk/edit',$article->id)}}">Cikk szerkesztése</a>

        {!! Form::open(['method' => 'DELETE', 'url' => 'cikk/delete/'.$article->id, 'style' => 'display: inline;']) !!} 
        <a type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete-{{ $article->id }}">Törlés</a>
        <div class="modal fade" id="modal-delete-{{ $article->id}}" tabIndex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            ×
                        </button>
                    </div>
                    <div class="modal-body">
                        <p class="lead"><i class="fa fa-question-circle fa-lg"></i>  Valóban törölhetjük?:: <code> {{ $article->title }}</code></p>
                    </div>
                    <div class="modal-footer">                        

                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Mégse</button>
                        <button class="btn btn-danger btn-sm" >Igen, törölhetjük</button>
                        {!! Form::close() !!}    

                    </div>
                </div>
            </div>
        </div>
    </div>
    @endauth
</article>
@endforeach
@endsection